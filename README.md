```
 .-.  .-.    __     __    _            _   _                .-.  .-.
/   \/   \   \ \   / /_ _| | ___ _ __ | |_(_)_ __   ___    /   \/   \
\        /    \ \ / / _` | |/ _ \ '_ \| __| | '_ \ / _ \   \        /
 `\    /'      \ V / (_| | |  __/ | | | |_| | | | |  __/    `\    /'
   `\/'         \_/ \__,_|_|\___|_| |_|\__|_|_| |_|\___|      `\/'
```

# ❤️ Valentine ❤️

**Happy Valentine's Day!**

![Celebration](valentine.gif)

The ❤️ Valentine ❤️ provides functionality for sending valentines to other site
users.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/valentine).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/valentine).


## Table of contents

- Requirements
- Recommended modules
- Installation
- Configuration
- Maintainers


## Requirements

This module requires the following modules:

- [Service](https://www.drupal.org/project/service)


## Recommended modules

[Mobile Detect](https://www.drupal.org/project/mobile_detect): When enabled,
modal windows can use the full width of the screen when you use a mobile phone.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

- Navigate to Administration > Extend and enable the module.

- Configure the user permissions in Administration > People > Permissions:
  - *Administer Valentine*

    Users with this permission will configure module settings and entities.

- Customize the module settings in Administration > Configuration > People >
  ❤️ Valentine ❤️.


## Maintainers

- Oleksandr Horbatiuk - [lexhouk](https://www.drupal.org/u/lexhouk)
