<?php

namespace Drupal\valentine\Controller;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\service\ControllerBase;
use Drupal\service\EntityTypeManagerTrait;
use Drupal\service\StringTranslationTrait;

/**
 * Controller for valentine pages.
 */
class ValentineController extends ControllerBase {

  use EntityTypeManagerTrait;
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  protected function creation(): static {
    return $this->addStringTranslation()->addEntityTypeManager();
  }

  /**
   * Provides an add title callback for an entity type.
   */
  public function addTitle(): string {
    return $this->title($this->t('Create a valentine'));
  }

  /**
   * Provides a list title callback for an entity type.
   */
  public function listTitle(): string {
    return $this->title(
      $this->entityTypeManager()->getDefinition('valentine')
        ->getCollectionLabel(),
    );
  }

  /**
   * Provides a generic title callback for an entity type.
   *
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup $text
   *   The title text.
   */
  public function title(TranslatableMarkup $text): string {
    return '♥ ' . $text . ' ♥';
  }

}
