<?php

namespace Drupal\valentine\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Symfony\Component\Routing\Route;

/**
 * Determines access to routes based on module settings.
 */
class ValentineAccessCheck implements AccessInterface {

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a ValentineAccessCheck object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * Checks access.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check against.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(Route $route) {
    $required_status = filter_var(
      $route->getRequirement('_valentine_access'),
      FILTER_VALIDATE_BOOLEAN
    );

    $actual_status = !$this->configFactory->get('valentine.settings')
      ->get('disabled');

    return AccessResult::allowedIf($required_status === $actual_status);
  }

}
