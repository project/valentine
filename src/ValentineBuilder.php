<?php

namespace Drupal\valentine;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait as CoreStringTranslationTrait;
use Drupal\Core\Url;
use Drupal\service\ClassResolverBase;
use Drupal\service\ConfigFactoryTrait;
use Drupal\service\EntityTypeManagerTrait;
use Drupal\service\ExtensionPathResolverTrait;
use Drupal\service\JsonSerializationTrait;
use Drupal\service\RedirectDestinationTrait;
use Drupal\service\RouteMatchTrait;
use Drupal\service\StringTranslationTrait;
use Drupal\valentine\Form\ValentineSettingsForm;

/**
 * Provides hook wrappers.
 *
 * @internal
 *   This is an internal utility class wrapping hook implementations.
 */
class ValentineBuilder extends ClassResolverBase {

  use ConfigFactoryTrait;
  use CoreStringTranslationTrait;
  use EntityTypeManagerTrait;
  use ExtensionPathResolverTrait;
  use JsonSerializationTrait;
  use RedirectDestinationTrait;
  use RouteMatchTrait;

  use StringTranslationTrait {
    StringTranslationTrait::getStringTranslation insteadof CoreStringTranslationTrait;
  }

  /**
   * {@inheritdoc}
   */
  protected function creation(): static {
    return $this
      ->addConfigFactory()
      ->addEntityTypeManager()
      ->addExtensionPathResolver()
      ->addJsonSerialization()
      ->addRedirectDestination()
      ->addRouteMatch()
      ->addStringTranslation();
  }

  /**
   * Perform periodic actions.
   *
   * @see valentine_cron()
   */
  public function cron(): void {
    $count = $this->configFactory()->get('valentine.settings')->get('delete')
      ?? ValentineSettingsForm::DELETE;

    if ($count > 0) {
      /** @var \Drupal\valentine\Entity\ValentineStorageInterface $storage */
      $storage = $this->entityTypeManager()->getStorage('valentine');

      $ids = $storage->getInboxQuery(FALSE)
        ->sort('id')
        ->range(0, $count)
        ->execute();

      if ($ids) {
        $storage->delete($storage->loadMultiple($ids));
      }
    }
  }

  /**
   * Alter forms for field widgets provided by other modules.
   *
   * Restore the initial selection handler for the recipient field if some other
   * module overrides it.
   *
   * @param array $element
   *   The field widget form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $context
   *   An associative array.
   *
   * @see valentine_field_widget_single_element_form_alter()
   */
  public function alterFieldWidgetSingleElementForm(
    array &$element,
    FormStateInterface $form_state,
    array $context,
  ): void {
    if (
      isset($element['target_id']['#target_type']) &&
      $element['target_id']['#target_type'] === 'user' &&
      !$this->configFactory()->get('valentine.settings')->get('disabled')
    ) {
      /** @var \Drupal\Core\Field\FieldDefinitionInterface $field_definition */
      $field_definition = $context['items']->getFieldDefinition();

      if (
        $field_definition->getTargetEntityTypeId() === 'valentine' &&
        $field_definition->getName() === 'recipient'
      ) {
        $element['target_id']['#selection_handler'] = 'valentine_selection';
      }
    }
  }

  /**
   * Provide online user help.
   *
   * @param string $route_name
   *   For page-specific help, use the route name as identified in the module's
   *   routing.yml file.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   *
   * @see valentine_help()
   */
  public function help(
    string $route_name,
    RouteMatchInterface $route_match,
  ): string {
    if ($route_name === 'help.page.valentine') {
      $path = $this->extensionPathResolver()->getPath('module', 'valentine');

      return '<img src="/' . $path . '/valentine.gif" alt="' . $this->t('Celebration') . '" class="valentine" />' .
        '<h3>' . $this->t('About') . '</h3>' .
        '<p>' . $this->t('The ❤️ Valentine ❤️ module provides functionality for sending valentines to other site users.') . '</p>' .
        '<h3>' . $this->t('Uses') . '</h3>' .
        '<dl>' .
        '<dt>' . $this->t('Creation form') . '</dt>' .
        '<dd>' . $this->t('You should see a red button to open a form to prepare and send a new valentine when you go to any user page (for example, <em>/user/1</em>). This button can have the shape of a heart or a simple rectangle. Also, the button is placed in one of the four corners of the page. As for the creation form, you can write a message (required) in plain text and a signature (optional). So, if the last field is left blank then no one will know who sent the valentine (even users who have direct access to the database).') . '</dd>' .
        '<dt>' . $this->t('View messages') . '</dt>' .
        '<dd>' . $this->t('A similar button to open the page of received messages should be available on all other pages (in case when you have at least one message that was sent in the current year). The number of new messages should be contained in the button label when you have unread ones.') . '</dd>' .
        '<dt>' . $this->t('Clean up') . '</dt>' .
        '<dd>' . $this->t('Valentines from previous years may be removed automatically via cron when the relevant field on the settings page does not have zero value.') . '</dd>' .
        '</dl>';
    }
    elseif ($route_name === 'valentine.settings') {
      return '<p>' . $this->t('Configure style and position button for sending a valentine or reading received ones. Also, define the default message and manage the clean-up process.') . '</p>';
    }

    return '';
  }

  /**
   * Add attachments (typically assets) to a page before it is rendered.
   *
   * @param array &$attachments
   *   An array that you can add attachments to.
   *
   * @see valentine_page_attachments()
   */
  public function pageAttachments(array &$attachments): void {
    if (
      $this->routeMatch()->getRouteName() === 'help.page' &&
      $this->routeMatch()->getParameter('name') === 'valentine'
    ) {
      $attachments['#attached']['library'][] = 'valentine/help';
    }
  }

  /**
   * Add a renderable array to the top of the page.
   *
   * @param array $page_top
   *   A renderable array representing the top of the page.
   *
   * @see valentine_page_top()
   */
  public function pageTop(array &$page_top): void {
    $config = $this->configFactory()->get('valentine.settings');

    if ($config->get('disabled')) {
      return;
    }

    if (($id = $this->routeMatch()->getRawParameter('user')) !== NULL) {
      $url = Url::fromRoute('entity.valentine.add_form', ['user' => $id]);

      if ($url->access()) {
        $url->mergeOptions([
          'query' => $this->getRedirectDestination()->getAsArray(),
        ]);

        $text = t('Send a valentine');
        $width = ($height = 3) + 1;
      }
    }

    if (
      !isset($text) &&
      !str_starts_with($this->routeMatch()->getRouteName(), 'entity.valentine.')
    ) {
      /** @var \Drupal\valentine\Entity\ValentineStorageInterface $storage */
      $storage = $this->entityTypeManager()->getStorage('valentine');

      if (
        $storage->getInboxSize() > 0 &&
        ($url = Url::fromRoute('entity.valentine.collection'))->access()
      ) {
        $definition = $this->entityTypeManager()->getDefinition('valentine');

        $text = ($count = $storage->getInboxSize(TRUE)) > 0
          ? $definition->getCountLabel($count)
          : $definition->getCollectionLabel();

        $width = $height = 7;
      }
    }

    if (!isset($text)) {
      return;
    }

    $classes = ['use-ajax', 'valentine', 'simple'];

    if (!($simple = $config->get('simple'))) {
      $classes[] = 'invisible';
    }

    if (\Drupal::hasService('mobile_detect')) {
      /** @var \Mobile_Detect $mobile_detect */
      $mobile_detect = \Drupal::service('mobile_detect');

      if ($mobile_detect->isMobile()) {
        $width = $height = $mobile_detect->isTablet() ? 9 : 10;
      }
    }

    $position = $config->get('position') ?? ValentineSettingsForm::POSITION;

    $url->setOption('attributes', [
      'class' => $classes,
      'data-dialog-type' => 'modal',
      'data-dialog-options' => $this->jsonSerialization()->encode([
        'width' => $width . '0%',
        'height' => $height . '0%',
      ]),
      'style' => str_replace('-', ':0;', $position . '-'),
    ]);

    $link = Link::fromTextAndUrl($text, $url)->toRenderable();

    $link['#attached']['drupalSettings']['valentine'] = str_contains(
      $position,
      'top',
    );

    $link['#attached']['library'][] = 'valentine/' .
      ($simple ? 'base' : 'heart');

    $page_top['valentine'] = $link;
  }

}
