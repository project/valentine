<?php

namespace Drupal\valentine\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseDialogCommand;
use Drupal\Core\Ajax\MessageCommand;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\service\ContentEntityFormBase;
use Drupal\service\MessengerTrait;
use Drupal\service\RequestStackTrait;
use Drupal\service\StringTranslationTrait;

/**
 * Form handler for the valentine creation form.
 */
class ValentineForm extends ContentEntityFormBase {

  use MessengerTrait;
  use RequestStackTrait;
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  protected function creation(): static {
    return $this->addMessenger()->addRequestStack()->addStringTranslation();
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(
    array $form,
    FormStateInterface $form_state,
  ): array {
    $element = parent::actions($form, $form_state);

    $element['submit']['#value'] = $this->t('Send');
    $element['submit']['#ajax'] = ['callback' => [$this, 'ajaxCallback']];

    return $element;
  }

  /**
   * Ajax callback to close the modal and show a message.
   */
  public function ajaxCallback(): AjaxResponse {
    return (new AjaxResponse())
      ->addCommand(new CloseDialogCommand())
      ->addCommand(new MessageCommand($this->message()));
  }

  /**
   * Gets message.
   */
  protected function message(): TranslatableMarkup {
    /** @var \Drupal\valentine\Entity\ValentineInterface $valentine */
    $valentine = $this->entity;

    return $this->t(
      'The valentine for %name has been sent.',
      ['%name' => $valentine->getOwner()->getDisplayName()],
    );
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    if (!$this->getRequest()->isXmlHttpRequest()) {
      $this->messenger()->addStatus($this->message());
    }

    return parent::save($form, $form_state);
  }

}
