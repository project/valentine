<?php

namespace Drupal\valentine\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Valentine settings for this site.
 */
class ValentineSettingsForm extends ConfigFormBase {

  /**
   * The fields mapping.
   *
   * The mapping between configuration items (array keys) and form fields (array
   * values). If a key is a number then the names of the configuration item and
   * form field are identical.
   */
  public const array FIELDS = [
    'disabled' =>'enabled',
    'simple' => 'heart',
    'position',
    'message',
    'delete',
  ];

  /**
   * The default position.
   */
  public const string POSITION = 'right-bottom';

  /**
   * The number of valentines for removing by default.
   */
  public const int DELETE = 10;

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['valentine.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'valentine_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state,
  ): array {
    $config = $this->config('valentine.settings');

    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#description' => $this->t('Allow to create valentines and read received ones.'),
      '#default_value' => !$config->get('disabled'),
    ];

    $form['heart'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Heart style'),
      '#description' => $this->t('The shape of the module button will look like a heart. Otherwise, it is rectangular.'),
      '#default_value' => !$config->get('simple'),
    ];

    $form['position'] = [
      '#type' => 'radios',
      '#title' => $this->t('Position'),
      '#description' => $this->t('Select the page corner where the module button will be placed.'),
      '#options' => [
        'left-top' => $this->t('Top left'),
        'right-top' => $this->t('Top right'),
        'left-bottom' => $this->t('Bottom left'),
        'right-bottom' => $this->t('Bottom right'),
      ],
      '#default_value' => $config->get('position') ?? static::POSITION,
    ];

    $form['message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message'),
      '#description' => $this->t('The default text of the letter on the page of creating a valentine.'),
      '#default_value' => $config->get('message'),
    ];

    $form['delete'] = [
      '#type' => 'number',
      '#title' => $this->t('Clean up'),
      '#description' => $this->t('Number of valentines from previous years which will be removed per one cron run.'),
      '#default_value' => $config->get('delete') ?? static::DELETE,
      '#min' => 0,
      '#max' => 100,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(
    array &$form,
    FormStateInterface $form_state,
  ): void {
    $config = $this->config('valentine.settings');

    foreach (static::FIELDS as $key => $name) {
      $value = $form_state->getValue($name);

      if ($different = is_string($key)) {
        $value = !$value;
      }

      $config->set($different ? $key : $name, $value);
    }

    $config->save();

    parent::submitForm($form, $form_state);
  }

}
