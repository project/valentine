<?php

namespace Drupal\valentine\Plugin\EntityReferenceSelection;

use Drupal\Core\Entity\Attribute\EntityReferenceSelection;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\user\Plugin\EntityReferenceSelection\UserSelection;

/**
 * Provides specific access control for the user entity type.
 */
#[EntityReferenceSelection(
  id: 'valentine_selection',
  label: new TranslatableMarkup('User selection'),
  entity_types: ['user'],
  group: 'valentine_selection',
  weight: 2,
)]
class ValentineSelection extends UserSelection {

  /**
   * {@inheritdoc}
   */
  protected function buildEntityQuery(
    $match = NULL,
    $match_operator = 'CONTAINS',
  ): QueryInterface {
    return parent::buildEntityQuery($match, $match_operator)
      ->condition('uid', $this->currentUser->id(), '<>');
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      ...parent::defaultConfiguration(),
      'include_anonymous' => FALSE,
    ];
  }

}
