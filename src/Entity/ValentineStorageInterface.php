<?php

namespace Drupal\valentine\Entity;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Entity\Query\QueryInterface;

/**
 * Defines an interface for valentine entity storage classes.
 */
interface ValentineStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets an entity query instance limited by the current year.
   *
   * @param bool $access_check
   *   (optional) Whether access check is requested or not. Defaults to TRUE.
   */
  public function getInboxQuery(bool $access_check = TRUE): QueryInterface;

  /**
   * Gets the number of messages.
   *
   * @param bool $close
   *   (optional) TRUE, if read messages should be excluded. Defaults to FALSE.
   */
  public function getInboxSize(bool $close = FALSE): int;

}
