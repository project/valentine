<?php

namespace Drupal\valentine\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a valentine entity.
 */
interface ValentineInterface extends ContentEntityInterface, EntityOwnerInterface, EntityPublishedInterface {

  /**
   * Gets the valentine creation timestamp.
   */
  public function created(): int;

  /**
   * Returns whether the valentine is closed.
   */
  public function isClosed(): bool;

  /**
   * Gets letter content.
   */
  public function message(): string;

  /**
   * Sets the valentine as opened.
   */
  public function setOpened(): static;

  /**
   * Gets signature of a letter.
   */
  public function signature(): string;

}
