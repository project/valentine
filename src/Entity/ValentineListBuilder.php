<?php

namespace Drupal\valentine\Entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Render\Markup;
use Drupal\service\EntityListBuilderBase;
use Drupal\service\RequestStackTrait;
use Drupal\service\StringTranslationTrait;

/**
 * Defines a class to build a listing of valentine entities.
 *
 * @see \Drupal\valentine\Entity\Valentine
 */
class ValentineListBuilder extends EntityListBuilderBase {

  use RequestStackTrait;
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  protected function creation(): static {
    $instance = $this->addStringTranslation()->addRequestStack();

    if (($count = $instance->getStorage()->getInboxSize()) < 50) {
      $instance->limit = 10;
    }
    elseif ($count < 100) {
      $instance->limit = 20;
    }
    else {
      $instance->limit = 30;
    }

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityIds(): array {
    $query = $this->getStorage()->getInboxQuery();

    if ($this->limit) {
      $query->pager($this->limit);
    }

    return $query->sort('close', 'DESC')->sort('id', 'DESC')->execute();
  }

  /**
   * Gets the entity storage.
   */
  public function getStorage(): ValentineStorageInterface {
    /** @var \Drupal\valentine\Entity\ValentineStorageInterface $storage */
    $storage = parent::getStorage();

    return $storage;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    return [
      'new' => $this->t('New'),
      'message' => $this->t('Message'),
      'signature' => $this->t('Signature'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\valentine\Entity\ValentineInterface $valentine */
    $valentine = $entity;

    if ($closed = $valentine->isClosed()) {
      $valentine->setOpened()->save();
    }

    return [
      'new' => $closed ? $this->t('Yes') : $this->t('No'),
      'message' => Markup::create(
        str_replace(PHP_EOL, '<br />', $valentine->message()),
      ),
      'signature' => $valentine->signature(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function render(): array {
    $build = parent::render();

    $build['table']['#empty'] = $this->t('No valentines.');

    if (isset($build['pager'])) {
      $request = $this->requestStack()->getCurrentRequest();

      if ($request !== NULL && $request->isXmlHttpRequest()) {
        $build['pager']['#route_name'] = 'entity.valentine.collection';
      }
    }

    return $build;
  }

}
