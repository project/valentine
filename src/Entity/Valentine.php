<?php

namespace Drupal\valentine\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the valentine entity class.
 *
 * @ContentEntityType(
 *   id = "valentine",
 *   label = @Translation("Valentine"),
 *   label_collection = @Translation("Valentines"),
 *   label_singular = @Translation("valentine"),
 *   label_plural = @Translation("valentines"),
 *   label_count = @PluralTranslation(
 *     singular = "@count valentine",
 *     plural = "@count valentines"
 *   ),
 *   handlers = {
 *     "storage" = "Drupal\valentine\Entity\ValentineStorage",
 *     "access" = "Drupal\valentine\Entity\ValentineAccessControlHandler",
 *     "list_builder" = "Drupal\valentine\Entity\ValentineListBuilder",
 *     "form" = {
 *       "default" = "Drupal\valentine\Form\ValentineForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\valentine\Entity\ValentineRouteProvider"
 *     }
 *   },
 *   admin_permission = "administer valentine",
 *   base_table = "valentine",
 *   data_table = "valentine_field_data",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "uid" = "recipient",
 *     "owner" = "recipient",
 *     "status" = "close",
 *     "published" = "close"
 *   },
 *   common_reference_target = TRUE,
 *   links = {
 *     "add-form" = "/user/{user}/valentine",
 *     "collection" = "/valentine"
 *   }
 * )
 */
class Valentine extends ContentEntityBase implements ValentineInterface {

  use EntityOwnerTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    /** @var \Drupal\Core\Field\BaseFieldDefinition[] $fields */
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields += static::ownerBaseFieldDefinitions($entity_type);
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['recipient']
      ->setLabel(t('Recipient'))
      ->setSetting('handler', 'valentine_selection')
      ->setSetting('handler_settings', [
        'sort' => [
          'field' => 'name',
          'direction' => 'ASC',
        ],
        'auto_create' => FALSE,
        'auto_create_bundle' => NULL,
        'filter' => [
          'type' => '_none',
          'role' => NULL,
        ],
        'include_anonymous' => FALSE,
      ])
      ->setRequired(TRUE)
      ->setReadOnly(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 0,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'match_limit' => 10,
          'size' => 60,
          'placeholder' => '',
        ],
      ]);

    $fields['message'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Message'))
      ->setDefaultValueCallback(static::class . '::getDefaultMessage')
      ->setRequired(TRUE)
      ->setReadOnly(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
        'weight' => 1,
        'settings' => [
          'rows' => 3,
          'placeholder' => '',
        ],
      ]);

    $fields['signature'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Signature'))
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 2,
        'settings' => [
          'size' => 60,
          'placeholder' => '',
        ],
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the valentine was created.'))
      ->setRequired(TRUE)
      ->setReadOnly(TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function created(): int {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public static function getDefaultEntityOwner(): mixed {
    return \Drupal::routeMatch()->getRawParameter('user');
  }

  /**
   * Default value callback for 'message' base field.
   */
  public static function getDefaultMessage(): mixed {
    return \Drupal::config('valentine.settings')->get('message');
  }

  /**
   * {@inheritdoc}
   */
  public function isClosed(): bool {
    return $this->isPublished();
  }

  /**
   * {@inheritdoc}
   */
  public function message(): string {
    return (string) $this->get('message')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage): void {
    parent::preSave($storage);

    $this->get('message')->value = strip_tags($this->message());
  }

  /**
   * {@inheritdoc}
   */
  public function setOpened(): static {
    return $this->setUnpublished();
  }

  /**
   * {@inheritdoc}
   */
  public function signature(): string {
    return (string) $this->get('signature')->value;
  }

}
