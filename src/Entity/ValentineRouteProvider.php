<?php

namespace Drupal\valentine\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider;
use Drupal\valentine\Controller\ValentineController;
use Symfony\Component\Routing\Route;

/**
 * Provides routes for the valentine entity.
 */
class ValentineRouteProvider extends DefaultHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  protected function getAddFormRoute(EntityTypeInterface $entity_type): ?Route {
    if (($route = parent::getAddFormRoute($entity_type)) !== NULL) {
      $items = (array) $route->getOption('parameters');
      $items['user'] = ['type' => 'entity:user'];

      $route
        ->setDefault(
          '_title_callback',
          ValentineController::class . '::addTitle',
        )
        ->setRequirement('_valentine_access', 'TRUE')
        ->setRequirement('user', '\d+')
        ->setOption('parameters', $items);
    }

    return $route;
  }

  /**
   * {@inheritdoc}
   */
  protected function getCollectionRoute(EntityTypeInterface $entity_type): ?Route {
    if (($route = parent::getCollectionRoute($entity_type)) !== NULL) {
      $items = $route->getDefaults();

      unset(
        $items['_title'],
        $items['_title_arguments'],
        $items['_title_context'],
      );

      $route
        ->setDefaults($items)
        ->setDefault(
          '_title_callback',
          ValentineController::class . '::listTitle',
        );

      $items = $route->getRequirements();

      unset($items['_permission']);

      $route
        ->setRequirements($items)
        ->setRequirement('_user_is_logged_in', 'TRUE')
        ->setRequirement('_valentine_access', 'TRUE');
    }

    return $route;
  }

}
