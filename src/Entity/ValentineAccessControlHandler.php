<?php

namespace Drupal\valentine\Entity;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityHandlerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\service\RouteMatchTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the access control handler for the valentine entity type.
 *
 * @see \Drupal\valentine\Entity\Valentine
 */
class ValentineAccessControlHandler extends EntityAccessControlHandler implements EntityHandlerInterface {

  use RouteMatchTrait;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(
    ContainerInterface $container,
    EntityTypeInterface $entity_type,
  ): static {
    return (new static($entity_type))->addRouteMatch($container);
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(
    AccountInterface $account,
    array $context,
    $entity_bundle = NULL,
  ): AccessResultInterface {
    return AccessResult::allowedIf(
      $account->isAuthenticated() &&
      $this->routeMatch()->getRawParameter('user') != $account->id(),
    );
  }

}
