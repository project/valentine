<?php

namespace Drupal\valentine\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\service\CurrentUserTrait;
use Drupal\service\TimeTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the storage handler class for valentines.
 *
 * This extends the base storage class, adding required special handling for
 * valentine entities.
 */
class ValentineStorage extends SqlContentEntityStorage implements ValentineStorageInterface {

  use CurrentUserTrait;
  use TimeTrait;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(
    ContainerInterface $container,
    EntityTypeInterface $entity_type
  ): static {
    return parent::createInstance($container, $entity_type)
      ->addCurrentUser($container)
      ->addTime();
  }

  /**
   * {@inheritdoc}
   */
  public function getInboxQuery(bool $access_check = TRUE): QueryInterface {
    $timestamp = $this->time()->getRequestTime();
    $timestamp = mktime(0, 0, 0, 1, 1, date('Y', $timestamp));

    $query = $this->getQuery()
      ->accessCheck($access_check)
      ->condition('created', $timestamp, $access_check ? '>=' : '<');

    if ($access_check) {
      $query->condition('recipient', $this->currentUser()->id());
    }

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function getInboxSize(bool $close = FALSE): int {
    $query = $this->getInboxQuery();

    if ($close) {
      $query->condition('close', TRUE);
    }

    return $query->count()->execute();
  }

}
