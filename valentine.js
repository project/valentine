(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.valentine = {
    attach: function attach(context) {
      once('valentine', '.valentine', context).forEach(function (value) {
        var $element = $(value);
        var width = $element.width() + 2 * $element.height();
        var prefixes = ['', '-half'];

        prefixes.forEach(function (prefix, divider) {
          document.documentElement.style.setProperty(
            '--valentine' + prefix + '-size',
            Math.round(width / (divider + 1)) + 'px'
          );
        });

        if (drupalSettings.valentine) {
          $element.css('top', Math.round(width / 20) + 'px');
        }
        else  {
          $element.css('bottom', -Math.round(width / 6) + 'px');
        }

        $element.removeClass('simple invisible').addClass('heart');
      });
    }
  }
})(jQuery, Drupal, drupalSettings);
